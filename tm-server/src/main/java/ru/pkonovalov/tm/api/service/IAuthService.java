package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role... roles);

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
