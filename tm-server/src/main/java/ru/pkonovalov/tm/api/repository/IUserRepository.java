package ru.pkonovalov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO `app_user` (`id`, `login`, `passwordHash`, `role`, `email`, `locked`, `firstName`, `lastName`, `middleName`) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{role}, #{email}, #{locked}, #{firstName}, #{lastName}, #{middleName})")
    void add(@Nullable @Param("user") User user);

    @Insert({
            "<script>",
            "INSERT INTO `app_user` ",
            "(`id`, `login`, `passwordHash`, `role`, `email`, `locked`, `firstName`, `lastName`, `middleName`) ",
            "VALUES" +
                    "<foreach item='e' collection='userList' open='' separator=',' close=''>" +
                    "(" +
                    "#{e.id},",
            "#{e.login},",
            "#{e.passwordHash},",
            "#{e.role},",
            "#{e.email},",
            "#{e.locked},",
            "#{e.firstName},",
            "#{e.lastName},",
            "#{e.middleName}"+
                    ")" +
                    "</foreach>",
            "</script>"})
    void addAll(@Nullable @Param("userList") List<User> userList);

    @Delete("DELETE FROM `app_user`")
    void clear();

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_user` WHERE `email` = #{email} LIMIT 1")
    boolean existsByEmail(@Nullable @Param("email") String email);

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_user` WHERE `id` = #{id} LIMIT 1")
    boolean existsById(@NotNull @Param("id") String id);

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_user` WHERE `login` = #{login} LIMIT 1")
    boolean existsByLogin(@Nullable @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM `app_user`")
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM `app_user` WHERE `id` = #{id}")
    User findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `app_user` WHERE `login` = #{login}")
    User findByLogin(@NotNull @Param("login") String login);

    @Delete("DELETE FROM `app_user` WHERE `id` = #{id}")
    void remove(@Nullable @Param("user") User user);

    @Delete("DELETE FROM `app_user` WHERE `id` = #{id}")
    void removeById(@Nullable @Param("id") String id);

    @Delete("DELETE FROM `app_user` WHERE `login` = #{login}")
    void removeByLogin(@Nullable @Param("login") String login);

    @Select("SELECT COUNT(*) FROM `app_user`")
    int size();

    @Update("UPDATE `app_user` SET `login` = #{login}, `passwordHash` = #{passwordHash}, `role` = #{role}" +
            "`email` = #{email}, `locked` = #{locked}, `firstName` = #{firstName}, `lastName` = #{lastName}, `middleName` = #{middleName}" +
            "WHERE `id` = #{id}")
    void update(@Nullable @Param("user") User user);

}
