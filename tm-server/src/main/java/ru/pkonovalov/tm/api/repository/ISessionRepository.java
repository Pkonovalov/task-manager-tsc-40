package ru.pkonovalov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO `app_session` (`id`, `timestamp`, `signature`, `userId`) " +
            "VALUES (#{session.id}, #{session.timestamp}, #{session.signature}, #{session.userId})")
    void add(@Nullable @Param("session") Session session);

    @Delete("DELETE FROM `app_session` WHERE `userId` = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `app_session`")
    void clearAll();

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_session` WHERE `userId` = #{userId} AND `id` = #{id} LIMIT 1")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_session` WHERE `id` = #{id} LIMIT 1")
    boolean contains(@NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM `app_session` WHERE `userId` = #{userId}")
    List<Session> findAllOfUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM `app_session`")
    List<Session> findAll();

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `id` = #{id}")
    Session findByIdAll(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `userId` = #{userId}")
    List<Session> findByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `id` = #{id} AND `userId` = #{userId}")
    Session findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM `app_session` WHERE `id` = #{session.id}")
    void remove(@Nullable @Param("session") Session session);

    @Delete("DELETE FROM `app_session` WHERE `id` = #{id} AND `userId` = #{userId}")
    void removeById(@NotNull @Param("userId") String userId, @Nullable @Param("id") String id);

    @Delete("DELETE FROM `app_session` WHERE `id` = #{id}")
    void removeByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT COUNT(*) FROM `app_session`")
    int sizeAll();

    @Select("SELECT COUNT(*) FROM `app_session` WHERE `userId` = #{userId}")
    int size(@NotNull @Param("userId") String userId);


    @Update("UPDATE `app_session` SET `timestamp` = #{session.timestamp}, `signature` = #{session.signature}, " +
            "`userId` = #{session.userId} WHERE `id` = #{session.id}")
    void update(@Nullable @Param("session") Session session);

}
