package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IService;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    void addAll(@Nullable List<Project> list);

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator);

    @Nullable
    Project add(@NotNull String userId, @Nullable String name, @Nullable String description);

    boolean existsByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @Nullable String name);

    void finishProjectById(@NotNull String userId, @Nullable String id);

    void finishProjectByIndex(@NotNull String userId, @NotNull Integer index);

    void finishProjectByName(@NotNull String userId, @Nullable String name);

    void setProjectStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    void setProjectStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    void setProjectStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    void startProjectById(@NotNull String userId, @Nullable String id);

    void startProjectByIndex(@NotNull String userId, @NotNull Integer index);

    void startProjectByName(@NotNull String userId, @Nullable String name);

    void updateProjectById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateProjectByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String description);

    void updateProjectByName(@NotNull String userId, @Nullable String name, @Nullable String nameNew, @Nullable String description);

}
