package ru.pkonovalov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.endpoint.IAdminEndpoint;
import ru.pkonovalov.tm.api.service.ServiceLocator;
import ru.pkonovalov.tm.component.Backup;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    private Backup backup;

    public AdminEndpoint() {
        super(null);
    }

    public AdminEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final Backup backup
    ) {
        super(serviceLocator);
        this.backup = backup;
    }

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @WebMethod
    public void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.loadJson();
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @WebMethod
    public void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.saveJson();
    }

}
