package ru.pkonovalov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.endpoint.ISessionEndpoint;
import ru.pkonovalov.tm.api.service.ServiceLocator;
import ru.pkonovalov.tm.exception.user.AccessDeniedException;
import ru.pkonovalov.tm.model.Session;
import ru.pkonovalov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return true;
        } catch (@NotNull final AccessDeniedException e) {
            return false;
        }
    }

    @Nullable
    @Override
    @WebMethod
    public User getUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        return serviceLocator.getSessionService().getUser(session);
    }

    @Nullable
    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }
}
