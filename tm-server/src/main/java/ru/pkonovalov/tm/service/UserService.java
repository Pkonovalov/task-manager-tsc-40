package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.IUserRepository;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.api.service.IUserService;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.exception.empty.EmptyEmailException;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.empty.EmptyLoginException;
import ru.pkonovalov.tm.exception.empty.EmptyPasswordException;
import ru.pkonovalov.tm.exception.entity.UserNotFoundException;
import ru.pkonovalov.tm.exception.user.EmailExistsException;
import ru.pkonovalov.tm.exception.user.LoginExistsException;
import ru.pkonovalov.tm.model.User;
import ru.pkonovalov.tm.util.HashUtil;

import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    public User add(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {

    }

    @SneakyThrows
    public void addAll(@Nullable List<User> list) {
        if (list == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.addAll(list);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.existsByLogin(login)) throw new LoginExistsException();
            if (userRepository.existsByEmail(email)) throw new EmailExistsException();
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            add(user);
            return user;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.existsByLogin(login)) throw new LoginExistsException();
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            userRepository.add(user);
            return user;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @SneakyThrows
    public boolean existsByEmail(@Nullable final String email) {
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.existsByEmail(email);
        }
    }

    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.existsById(id);
        }
    }

    @Override
    public @NotNull List<User> findAll(@NotNull String userId) {
        return null;
    }

    @SneakyThrows
    public boolean existsByLogin(@Nullable final String login) {
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.existsByLogin(login);
        }
    }

    @Nullable
    @SneakyThrows
    public List<User> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

    @Nullable
    @SneakyThrows
    public User findById(@NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findById(id);
        }
    }

    @Override
    public @Nullable User findById(@NotNull String userId, @NotNull String id) {
        return null;
    }

    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @SneakyThrows
    public void lockUserByLogin(@Nullable String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            update(user);
        }
    }

    @SneakyThrows
    public void remove(@Nullable final User user) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.remove(user);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeById(id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    public void removeByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @SneakyThrows
    public void setPassword(@NotNull final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            update(user);
        }

    }

    @SneakyThrows
    public int size() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.size();
        }
    }

    @Override
    public int size(@NotNull String userId) {
        return 0;
    }

    @SneakyThrows
    public void unlockUserByLogin(@Nullable String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
        }
    }

    @SneakyThrows
    public void update(@Nullable final User user) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            update(user);
        }
    }

}
