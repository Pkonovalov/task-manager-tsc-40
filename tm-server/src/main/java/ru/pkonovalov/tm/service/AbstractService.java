package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.api.IService;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.entity.EntityNotFoundException;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

}
