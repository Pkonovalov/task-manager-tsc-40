package ru.pkonovalov.tm.exception;

import org.jetbrains.annotations.Nullable;

public class AbstractException extends RuntimeException {

    public AbstractException(@Nullable final String s) {
        super(s);
    }

    public AbstractException(@Nullable final Throwable e) {
        super(e);
    }

}
