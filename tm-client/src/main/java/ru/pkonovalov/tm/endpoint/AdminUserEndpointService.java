package ru.pkonovalov.tm.endpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-03-12T16:43:55.698+03:00
 * Generated source version: 3.4.2
 */
@WebServiceClient(name = "AdminUserEndpointService",
        wsdlLocation = "http://localhost/8081/AdminUserEndpoint?WSDL",
        targetNamespace = "http://endpoint.tm.pkonovalov.ru/")
public class AdminUserEndpointService extends Service {

    public final static QName AdminUserEndpointPort = new QName("http://endpoint.tm.pkonovalov.ru/", "AdminUserEndpointPort");
    public final static QName SERVICE = new QName("http://endpoint.tm.pkonovalov.ru/", "AdminUserEndpointService");
    public final static URL WSDL_LOCATION;

    static {
        URL url = null;
        try {
            url = new URL("http://localhost/8081/AdminUserEndpoint?WSDL");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(AdminUserEndpointService.class.getName())
                    .log(java.util.logging.Level.INFO,
                            "Can not initialize the default wsdl from {0}", "http://localhost/8081/AdminUserEndpoint?WSDL");
        }
        WSDL_LOCATION = url;
    }

    public AdminUserEndpointService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public AdminUserEndpointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public AdminUserEndpointService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public AdminUserEndpointService(WebServiceFeature... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public AdminUserEndpointService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public AdminUserEndpointService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }


    /**
     * @return returns AdminUserEndpoint
     */
    @WebEndpoint(name = "AdminUserEndpointPort")
    public AdminUserEndpoint getAdminUserEndpointPort() {
        return super.getPort(AdminUserEndpointPort, AdminUserEndpoint.class);
    }

    /**
     * @param features A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return returns AdminUserEndpoint
     */
    @WebEndpoint(name = "AdminUserEndpointPort")
    public AdminUserEndpoint getAdminUserEndpointPort(WebServiceFeature... features) {
        return super.getPort(AdminUserEndpointPort, AdminUserEndpoint.class, features);
    }

}
