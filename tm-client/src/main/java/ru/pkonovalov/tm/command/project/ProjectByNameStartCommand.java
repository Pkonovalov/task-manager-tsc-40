package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByNameStartCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Start project by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-start-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        endpointLocator.getProjectEndpoint().startProjectByName(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
