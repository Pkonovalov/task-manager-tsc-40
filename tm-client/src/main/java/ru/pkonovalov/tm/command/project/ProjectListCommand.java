package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Project;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show project list";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @Override
    public void execute() {
        out.println("[PROJECT LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        @NotNull AtomicInteger index = new AtomicInteger(1);
        @Nullable final List<Project> list = endpointLocator.getProjectEndpoint().findProjectAll(endpointLocator.getSession());
        if (list == null) return;
        list.forEach((x) -> out.printf("%-3s | %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n",
                index.getAndIncrement(), x.getId(), x.getStatus(), x.getName(), x.getCreated(), x.getDateStart(), x.getDateFinish())
        );
    }

}
