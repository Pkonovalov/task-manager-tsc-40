package ru.pkonovalov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.other.ISaltSettings;

public interface HashUtil {

    @Nullable
    static String md5(@Nullable final String s) {
        if (s == null) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(s.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (@NotNull final java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    static String salt(
            @Nullable final ISaltSettings settings,
            @Nullable final String value
    ) {
        if (settings == null) return null;
        @Nullable final String secret = settings.getPasswordSecret();
        @Nullable final Integer iteration = settings.getPasswordIteration();
        return salt(value, iteration, secret);
    }

    @Nullable
    static String salt(
            @Nullable final String s,
            @NotNull Integer iteration,
            @NotNull String secret
    ) {
        if (s == null) return null;
        @Nullable String result = s;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

}