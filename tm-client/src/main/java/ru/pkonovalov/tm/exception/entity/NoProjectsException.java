package ru.pkonovalov.tm.exception.entity;

public class NoProjectsException extends RuntimeException {

    public NoProjectsException() {
        super("Error! No projects...");
    }

}
